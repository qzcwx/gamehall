import java.io.File;

import javax.swing.JFileChooser;

public class FileChooser {
	
	JFileChooser chooser = new JFileChooser();
	
	
	public FileChooser(JdbcMySql jdbcMySql) {
		chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle("Choose the path of the selected game");
		//    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		//		chooser.setAcceptAllFileFilterUsed(true);

		
		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
			System.out.println("getSelectedFile() : " + chooser.getSelectedFile());    
		} else {
			System.out.println("No Selection ");
		}
	}

	public File getSelectedFile(){
		return chooser.getSelectedFile();
	}  
}