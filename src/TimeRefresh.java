
import java.util.Timer;
import java.util.TimerTask;

/**
 * Simple demo that uses java.util.Timer to schedule a task to execute
 * once 5 seconds have passed.
 */

public class TimeRefresh {
	Timer timer;//瀹氫箟涓�釜瀹氭椂鍣�
	MyTask myTask;

	public TimeRefresh(int seconds, GameHall gameHall) {
		timer = new Timer();
		myTask = new MyTask(gameHall);
		timer.schedule(myTask, seconds*1000, seconds*1000);//瀹氫箟濂芥兂瑕佹墽琛岀殑浠诲姟鍜屾瘡娆℃墽琛岀殑鏃堕棿闂撮殧
	}

	class MyTask extends TimerTask {//瀹氫箟鑷繁鐨勪换鍔＄被,涓�畾瑕佺户鎵縏imerTask
		GameHall gameHall;
		int i = 1;
		public MyTask(GameHall ingameHall) {
			// TODO Auto-generated constructor stub
			gameHall = ingameHall;
		}

		public void run() {
			gameHall.RefreshPlatform();
			System.out.println("Automatic Refresh Platform " + i);
			i++;
		}
	}

	public void cancel() {
		myTask.cancel();
	}

	//    public static void main(String args[]) {
	//        System.out.println("About to schedule task.");
	//        new DataBaseReader(5);
	//        System.out.println("Task scheduled.");
	//    }
}