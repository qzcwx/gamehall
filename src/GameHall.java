import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.SpringLayout;
import javax.swing.WindowConstants;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;



/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class GameHall extends javax.swing.JFrame {

	private JMenuBar jMenuBar;
	private JLabel userInfor;	
	private JMenu jMenuHelp;
	private JMenu jMenuAccount;
	private JMenu jMenuPlatform;

	private JPanel rightPanel;
	private JPanel leftPanel;
	private JScrollPane upPanel;
	private JTree gameEntry;
	private JScrollPane treeScorllPane;

	private final static String loginWelcomeText= "Login";

	//	SocketServer gameHallsc;
	SocketThread t;

	int userID;
	int gameID;
	int roomID;
	int tableID;
	int seatID;

	JdbcMySql jdbcMySql;
	Container content;
	TimeRefresh timeFresh;

	//	private int numberOfGame;
	//	private String[] gameName;
	//	private int numberOfRoom;

	private int numberOfTable;
	private String updatestr;
	private String comMess;
	protected String visibelPathStr;
	String borderDisplay;
	String currentNode;
	//	private int numberOfSeat;
	private static boolean testGame = true;
	private String userNickname;
	private String userIP;
	private Process gameStatus;
	private static String userPort = "8000";

	/**
	 * Auto-generated main method to display this JFrame
	 */
	//	public static void main(String[] args) {
	//		SwingUtilities.invokeLater(new Runnable() {
	//			public void run() {
	//				GameHall inst = new GameHall(1);
	//				inst.setLocationRelativeTo(null);
	//				inst.setVisible(true);
	//			}
	//		});
	//	}

	public GameHall(int i, JdbcMySql inJdbcMySql) {
		jdbcMySql = inJdbcMySql;
		userID = i;

		System.out.println("New GameHall");

		// set the icon for window
		try {
			URL imagePath = new URL("file:GHall.png");		
			BufferedImage image = null;
			try {
				image = ImageIO.read(getClass().getResource("GHall.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.setIconImage(image);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}		
		initGUI();
	}

	private void initGUI() {
		content = getContentPane();
		content.setLayout(new BorderLayout());		
		visibelPathStr = null;
		try {
			// Set the status of User with ID to Available ('a')
			System.out.println("UPDATE UserTable SET status='a' WHERE user_id=" +
					userID +
			";");
			System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE UserTable SET status='a' WHERE user_id=" +
					userID +
			";"));

			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);			
			this.addWindowListener(new java.awt.event.WindowAdapter() {
				public void windowClosing(WindowEvent winEvt) {
					timeFresh.cancel();
					System.exit(0); 
				}
			});

			AddMenuBar();

			leftPanel = new JPanel();
			RefreshLeftPanel();	
			// Add Left Panel
			content.add(leftPanel,BorderLayout.WEST);

			// Right Panel			
			rightPanel = new JPanel();
			JScrollPane scrollPane = new JScrollPane(rightPanel);

			// 3 tables in a low at most
			rightPanel.setLayout(new GridLayout(numberOfTable/3,3));
			rightPanel.setBorder(BorderFactory.createTitledBorder("Please first select a game and then a room"));

			scrollPane.setPreferredSize(new java.awt.Dimension(300, 400));
			content.add(scrollPane, BorderLayout.CENTER);

			// Setting of Window
			int windowW = 1000;
			int windowH = 800;
			setSize(windowW, windowH);

			setVisible(true);
			this.setTitle("Game Platform by Group 33");


			timeFresh = new TimeRefresh(30,this);

		} catch (Exception e) {
			//add your error handling code here
			e.printStackTrace();
		}
	}

	/**
	 * The method for adding components to the Left Panel
	 * 
	 * @throws SQLException
	 */
	private void RefreshLeftPanel() throws SQLException {
		//	remove all the components in the left panel
		leftPanel.removeAll();
		System.out.println("Left Panel Remove All");

		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));

		// Up Panel
		System.out.println("Acquire Current User's information");
		upPanel = new JScrollPane(AcquireUserInformationTable());

		upPanel.setBorder(BorderFactory.createTitledBorder("User Information"));

		upPanel.setAutoscrolls(true);
		upPanel.setPreferredSize(new java.awt.Dimension(400,200));

		leftPanel.add(upPanel);

		// Down Panel
		ArrayList<Object> hierarchy = buildHierachyObject();

		DefaultMutableTreeNode root1 = processHierarchy(hierarchy);
		gameEntry = new JTree(root1);
		treeScorllPane = new JScrollPane(gameEntry);
		gameEntry.setRootVisible(false);

		//		gameEntry.expandPath(visibelPathStr);

		System.out.println("Current Visit Path ="+visibelPathStr);

		if (visibelPathStr!=null){
			TreePath previousPath = getPathFromStr1(visibelPathStr);
			System.out.println("Tree Path to be expanded =" + previousPath);			
			gameEntry.expandPath(previousPath);	
			gameEntry.makeVisible(previousPath);
			gameEntry.setSelectionPath(previousPath);
		}

		//		gameEntry.makeVisible(visibelPathStr);
		//		gameEntry.setSelectionPath(visibelPathStr);


		// add the listener of Selection of Tree, and update the right panel accordingly
		gameEntry.addTreeSelectionListener(new TreeSelectionListener() {

			public void valueChanged(TreeSelectionEvent e) {


				currentNode = e.getNewLeadSelectionPath().getLastPathComponent().toString();
				System.out.println(currentNode);

				//				TreePath visibelPathStr = new TreePath(gameEntry.getModel().getPathToRoot(e.getNewLeadSelectionPath()));
				visibelPathStr = e.getPath().toString();
				// border display at the top of right panel
				borderDisplay = currentNode;

				String parentNode = e.getNewLeadSelectionPath().getParentPath().getLastPathComponent().toString();
				if (parentNode!="All Games"){
					System.out.println(parentNode);
					borderDisplay = borderDisplay + ", " + parentNode;
					rightPanel.removeAll();

					// extract the room id from string "currentNode"
					String[] splitString = currentNode.split(" ");
					roomID = Integer.parseInt(splitString[1]);
					System.out.println("roomID=" + roomID);

					ResultSet rs;
					ResultSet gameIDrs;
					gameID=0;

					try {
						System.out.println("SELECT game_id from GameTable where game_name = " +
								"'" +parentNode +"'"+
						";");
						gameIDrs =jdbcMySql.executeQuery("SELECT game_id from GameTable where game_name = " +
								"'" +parentNode +"'"+
						";");

						if (gameIDrs.first()){
							gameID = Integer.parseInt(gameIDrs.getString(1));
							System.out.println("game ID = "+gameID);
							//							System.out.println("Acquire All Users' Information for current room and its relevant game");
							//							upPanel = new JScrollPane(AcquireAllUserInfoTable());
						}else{
							System.err.println("Error when retrive game ID from Game Table");
							System.exit(-1);
						}

						rs=	jdbcMySql.executeQuery("SELECT table_id, user1_id, user2_id, user3_id FROM TBTable where room_id = " +
								roomID +
								" and game_id = " +
								gameID +
						";");
						int TableID = 0;
						if (rs.first()){
							do{
								rightPanel.add(
										newTableRightPanel(
												Integer.parseInt(rs.getString(2)),// user1_id
												Integer.parseInt(rs.getString(3)),// user2_id
												Integer.parseInt(rs.getString(4)),// user3_id
												++TableID
										));		
							}while(rs.next());
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}else{
					rightPanel.removeAll();
				}
				rightPanel.revalidate();
				rightPanel.setBorder(BorderFactory.createTitledBorder(borderDisplay));
			}
		});

		treeScorllPane.setBorder(BorderFactory.createTitledBorder("Game Entrance"));

		leftPanel.add(treeScorllPane);

		leftPanel.revalidate();
		leftPanel.repaint();
	}

	/**
	 * 
	 * @param Path
	 * @return
	 */
	private TreePath getPathFromStr1(String Path) {
		TreePath[] allTreePath = getPaths(gameEntry);
		System.out.println("String Segment before split is "+Path);
		String str[] = Path.split("\\(");
		String strSeg = str[0];
		System.out.println("String Segment after split is "+strSeg);
		for (int i = 0; i < allTreePath.length; i++){
			if (allTreePath[i].toString().contains(strSeg)){
				System.out.println("Path has been founded.");
				return allTreePath[i];
			}
		}
		System.out.println("Path Not Found!");
		return null;
	}

	/**
	 * Get all the tree-path going with specified tree 
	 * @param tree
	 * @return
	 */
	public TreePath[] getPaths(JTree tree) {
		TreeNode root = (TreeNode)tree.getModel().getRoot();

		// Create array to hold the treepaths
		List list = new ArrayList();

		// Traverse tree from root adding treepaths for all nodes to list
		getPaths(tree, new TreePath(root), list);

		// Convert list to array
		return (TreePath[])list.toArray(new TreePath[list.size()]);
	}

	/**
	 * Add all the subsequent tree-path going after given tree-path, return the result as list
	 * @param tree
	 * @param parent
	 * @param list
	 */
	public void getPaths(JTree tree, TreePath parent, List list) {
		// Add node to list
		list.add(parent);

		// Create paths for all children
		TreeNode node = (TreeNode)parent.getLastPathComponent();
		if (node.getChildCount() >= 0) {
			for (Enumeration e=node.children(); e.hasMoreElements(); ) {
				TreeNode n = (TreeNode)e.nextElement();
				TreePath path = parent.pathByAddingChild(n);
				getPaths(tree, path, list);
			}
		}
	}


	/**
	 * Subject to the string variable, determine 
	 * 
	 * @param inVisibelPathStr
	 * @return
	 */
	private TreePath getPathFromStr(String inVisibelPathStr) {
		TreeModel model = gameEntry.getModel();
		Object root = model.getRoot();
		System.out.println("String Segment before split is "+inVisibelPathStr);
		String str[] = inVisibelPathStr.split("\\(");
		String strSeg = str[0];
		System.out.println("String Segment after split is "+strSeg);
		return (TreePath) walkJTree(model,root, strSeg);
	}


	/**
	 * Traverse JTree in a recursive way:
	 * Usage: 
	 *		TreeModel model = tree.getModel();
	 *		Object root = model.getRoot();
	 *		walkJTree( model, root );
	 *
	 * @param model
	 * @param o
	 * @param strSeg 
	 */
	protected Object walkJTree( TreeModel model, Object o, String strSeg )
	{
		int count = model.getChildCount(o);
		for( int i=0;  i < count;  i++ )
		{
			Object child = model.getChild(o, i );
			System.out.println("Current Node is " + child.toString());
			String currentNodeStr = child.toString();
			if (currentNodeStr.contains(strSeg))
				return child;	
			else
				return walkJTree( model, child, strSeg);	        
		}
		return null;
	}

	/** 
	 * Build up the tree-structured object
	 * @return ArrayList<Object>
	 * @throws SQLException
	 */
	private ArrayList<Object> buildHierachyObject() throws SQLException {
		ArrayList<Object>  hierarchy = new ArrayList<Object>(),temp;

		hierarchy.add("All Games");	
		// query the amount of games
		ResultSet rsGame = jdbcMySql.executeQuery("SELECT game_id,game_name from GameTable;");
		if (rsGame.first()){			
			do{
				temp = new ArrayList<Object>();	

				// add the name of games
				temp.add(rsGame.getString(2));
				{					
					ResultSet rsRoom = jdbcMySql.executeQuery("SELECT room_id from RoomTable where game_id=" +
							rsGame.getString(1) + // game_id
					";");										

					if (rsRoom.first()){
						do{
							// the number of online player in a certain room
							ResultSet rsOnline = jdbcMySql.executeQuery("SELECT SUM(IF(user1_id<>0,1,0)+IF(user2_id<>0,1,0)+IF(user3_id<>0,1,0)) from TBTable where game_id=" +
									rsGame.getString(1) +
									" AND room_id=" +
									rsRoom.getString(1) +
							"; ");

							// the number of tables
							ResultSet rsTotal = jdbcMySql.executeQuery("SELECT COUNT(*) from TBTable where game_id=" +
									rsGame.getString(1) +
									" AND room_id=" +
									rsRoom.getString(1) +
							";");

							if (rsTotal.first()&&rsOnline.first()){
								numberOfTable = Integer.parseInt(rsTotal.getString(1));
								temp.add("Room "+rsRoom.getString(1)+" ("+rsOnline.getString(1)+"/"+(Integer.parseInt(rsTotal.getString(1))*3)+")");
							}else{
								System.err.println("ERROR in counting entries of TBTable");
								System.exit(-1);
							}
						}while(rsRoom.next());											
					}else{
						System.err.println("ERROR in querying the entries of room table");
						System.exit(-1);
					}
				}
				hierarchy.add(temp);
			}while(rsGame.next());
		}
		else{
			System.err.println("ERROR in counting the entries of game table");
			System.exit(-1);
		}
		return hierarchy;
	}

	/** 
	 * retrieval the user information from database, and display them in a label with HTML formation.
	 * @param ID
	 * @return
	 * @throws SQLException
	 */
	String AcquireUserInformation() throws SQLException {
		ResultSet rs = jdbcMySql.executeQuery("SELECT account, nickname, sex, Age, status from UserTable WHERE user_id = " +
				userID +
		";");

		ResultSetMetaData md = rs.getMetaData();

		int cols = md.getColumnCount();
		String resultString = new String();
		if (rs.first()) {
			System.out.println(rs.getString(1));
			//			 <html> <body> line <br> newline </body> </html>
			resultString += "<html> <body>";

			for (int i = 0; i < cols; i++) {
				resultString += md.getColumnName(i + 1) + ": \t" + rs.getString(i+1) + "<br>";
			}
			resultString += "</body> </html>";

			//			this.dispose();
		} else {
			new MessageBox("User Name/Password incorrect!");
		}
		return resultString;
	}

	/**
	 * get the current user's information as table formation
	 */
	private JTable AcquireUserInformationTable() throws SQLException{
		ResultSet rs = jdbcMySql.executeQuery("SELECT account, nickname, sex, Age, status from UserTable WHERE user_id = " +
				userID +
		";");

		ResultSetMetaData md = rs.getMetaData();

		int cols = md.getColumnCount();
		String col[] = new String[cols];
		String data[][] = new String[1][cols];

		if (rs.first()) {
			System.out.println(rs.getString(1));
			for (int i = 0; i < cols; i++) {
				col[i]=md.getColumnName(i + 1);
				data[0][i]=rs.getString(i+1);
			}
		} else {
			new MessageBox("User Name/Password incorrect!");
		}

		DefaultTableModel model = new DefaultTableModel(data,col);

		JTable table = new JTable(model){
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column){
				Component returnComp = super.prepareRenderer(renderer, row, column);
				//				Color alternateColor = new Color(252,242,206);
				Color alternateColor =  new Color(200, 200, 200);
				Color whiteColor = Color.WHITE;
				if (!returnComp.getBackground().equals(getSelectionBackground())){
					Color bg = (row % 2 == 0 ? alternateColor : whiteColor);
					returnComp .setBackground(bg);
					bg = null;
				}
				return returnComp;
			}
		};
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		return table;
	}

	/**
	 * Get All user's information with specific game id and room id. Meanwhile, the entry associated with current logging in user is highlighed
	 * @throws SQLException 
	 * x
	 */
	private JTable AcquireAllUserInfoTable() throws SQLException{
		System.out.println("SELECT UserTable.nickname, UserTable.sex, UserTable.Age, UserGameTable.score FROM UserTable INNER JOIN UserGameTable ON UserTable.user_id=UserGameTable.user_id WHERE UserGameTable.game_id=" +
				gameID +
				" and UserGameTable.room_id=" +
				roomID +
		" ORDER BY UserTable.user_id;");
		ResultSet rs = jdbcMySql.executeQuery("SELECT UserTable.nickname, UserTable.sex, UserTable.Age, UserGameTable.score FROM UserTable INNER JOIN UserGameTable ON UserTable.user_id=UserGameTable.user_id WHERE UserGameTable.game_id=" +
				gameID +
				" and UserGameTable.room_id=" +
				roomID +
		" ORDER BY UserTable.user_id;");

		ResultSetMetaData md = rs.getMetaData();

		int cols = md.getColumnCount()-1;
		System.out.println("SELECT COUNT(*) FROM UserTable INNER JOIN UserGameTable ON UserTable.user_id=UserGameTable.user_id WHERE UserGameTable.game_id=" +
				gameID +
				" and UserGameTable.room_id=" +
				roomID +
		";");
		ResultSet rowCountRS = jdbcMySql.executeQuery("SELECT COUNT(*) FROM UserTable INNER JOIN UserGameTable ON UserTable.user_id=UserGameTable.user_id WHERE UserGameTable.game_id=" +
				gameID +
				" and UserGameTable.room_id=" +
				roomID +
		";");
		int rows = 0;
		if (rowCountRS.first()){
			rows= Integer.parseInt(rowCountRS.getString(1));
		}else{
			System.err.println("ERROR: Fail to count the rows of the all user information");
		}

		System.out.println("Number of rows="+rows);		
		String col[] = new String[cols];
		String data[][] = new String[rows][cols];

		if (rs.first()) {
			int currentRows=0;
			do {
				System.out.println(rs.getString(1));
				for (int i = 0; i < cols; i++) {
					col[i]=md.getColumnName(i + 1);
					data[currentRows][i]=rs.getString(i+1);
				}
				currentRows++;
			}while(rs.next());			
		} else {
			new MessageBox("ERROR: User Information Not Found");
		}

		DefaultTableModel model = new DefaultTableModel(data,col);

		JTable table = new JTable(model){
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column){
				Component returnComp = super.prepareRenderer(renderer, row, column);
				//				Color alternateColor = new Color(252,242,206);
				Color alternateColor =  new Color(200, 200, 200);
				Color whiteColor = Color.WHITE;
				if (!returnComp.getBackground().equals(getSelectionBackground())){
					Color bg = (row % 2 == 0 ? alternateColor : whiteColor);
					returnComp .setBackground(bg);
					bg = null;
				}
				return returnComp;
			}
		};
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		return table;		
	}

	/** 
	 * Add menu bar to the panel
	 */
	public void AddMenuBar(){

		jMenuBar = new JMenuBar();
		setJMenuBar(jMenuBar);

		jMenuPlatform = new JMenu("Platform");
		jMenuBar.add(jMenuPlatform);

		JMenuItem jMenuItemRefresh = new JMenuItem("Refresh"); 		
		jMenuPlatform.add(jMenuItemRefresh);
		jMenuItemRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.out.println("Refresh Selected");
				RefreshPlatform();
			}      
		});

		JMenuItem jMenuItemExit = new JMenuItem("Exit");
		jMenuPlatform.add(jMenuItemExit);
		jMenuItemExit.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent evt) {
				System.out.println("Exit Press");
				ExitItemSelected(evt);
			}
		});

		jMenuAccount = new JMenu("Account");
		jMenuBar.add(jMenuAccount);

		JMenuItem jMenuItemSwitchUser = new JMenuItem("Switch User"); 
		jMenuAccount.add(jMenuItemSwitchUser);
		jMenuItemSwitchUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.out.println("Switch User Selected");
				SwitchUserItemSelected(evt);
			}      
		});

		jMenuHelp = new JMenu("Help");
		jMenuBar.add(jMenuHelp);
		JMenuItem jMenuItemUserManual = new JMenuItem("User Manual"); 
		jMenuHelp.add(jMenuItemUserManual);
		jMenuItemUserManual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.out.println("User Manual Selected");
				UserManualItemSelected(evt);
			}      
		});

		JMenuItem jMenuItemAbout = new JMenuItem("About"); 
		jMenuHelp.add(jMenuItemAbout);
		jMenuItemAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.out.println("About Selected");
				AboutItemSelected(evt);
			}      
		});
	}

	protected void AboutItemSelected(ActionEvent evt) {
		AboutDialog inst = new AboutDialog();
		inst.setLocationRelativeTo(null);
		inst.setVisible(true);
	}

	protected void UserManualItemSelected(ActionEvent evt) {
		try {
			Desktop.getDesktop().open(new File("./manual.pdf"));		
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	protected void SwitchUserItemSelected(ActionEvent evt) {
		this.dispose();
		//	Invoke Login Window
		Login loginWindow=null;
		try {
			loginWindow = new Login(loginWelcomeText,jdbcMySql);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		loginWindow.setLocationRelativeTo(null);
		loginWindow.setVisible(true);
	}

	protected void ExitItemSelected(ActionEvent evt) {
		this.dispose();
	}

	protected void RefreshPlatform() {		
		try {
			RefreshLeftPanel();
			RefreshRightPanel(roomID, gameID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void RefreshRightPanel() {
		rightPanel.removeAll();
		rightPanel.setBorder(BorderFactory.createTitledBorder("Please first select a game and then a room"));
	}

	/** 
	 * Small routine that will make node out of the first entry
	 *  in the array, then make nodes out of subsequent entries
	 *  and make them child nodes of the first one. The process is
	 *  repeated recursively for entries that are arrays.
	 */
	private DefaultMutableTreeNode processHierarchy(ArrayList<Object> hierarchy) {
		DefaultMutableTreeNode node =
			new DefaultMutableTreeNode(hierarchy.get(0));
		DefaultMutableTreeNode child;
		for(int i=1; i < hierarchy.size(); i++) {
			Object nodeSpecifier = hierarchy.get(i);
			//		if (nodeSpecifier instanceof Object[])  // Ie node with children/
			if (nodeSpecifier instanceof ArrayList<?>)  // Ie node with children
				child = processHierarchy((ArrayList<Object>)nodeSpecifier);
			else
				child = new DefaultMutableTreeNode(nodeSpecifier); // Ie Leaf
			node.add(child);
		}
		return(node);
	}

	/** 
	 * The process of entering game of selecting which kind of game to evoke
	 * 
	 * @throws SQLException 
	 * @throws IOException 
	 */
	protected void enterGame() throws SQLException, IOException {

		if (testGame ==true){
			enterTestGame();
		}else{
			enter14Game();
		}
	}

	/** 
	 * The process of entering test game:
	 * 1. if the relevant entry in UserGameTable doesn't exist, create one
	 * 2. 	 else open the game according to the path stored in the UserGameTable
	 * 	2.1.		if the path is incorrect, then invoke a file chooser update the path in table and then execute the program
	 * 3. set status to be gaming
	 * 4. update the TBTable to set the user_id
	 * 5. Associate the seat with the nickname of user
	 * 6. refresh the Left Panel, in order to display correct amount of online users
	 * 
	 * @throws SQLException 
	 * @throws IOException 
	 */
	private void enterTestGame() throws SQLException, IOException {
		System.out.println("Enter Game");
		System.out.println("seat="+seatID+", tableID="+tableID+", roomID="+roomID+", gameID="+gameID);

		//  1. check the existence of the relevant entry in UserGameTable
		ResultSet rs = jdbcMySql.executeQuery("SELECT start_addr from UserGameTable where user_id = " +
				userID +
				" and game_id = " +
				gameID +
		"");
		System.out.println("rs ="+rs.first()+ ", user ="+userID + ", game ="+gameID);

		// Enable Multi-Thread Functionality for Socket
		//		RunSocket r = new RunSocket();
		t = new SocketThread(this, userID, seatID, tableID, roomID, gameID);
		t.start();
		//		gameHallsc = t.getServerSocket();

		//		SocketThread socketThread = new SocketThread();
		//		gameHallsc = socketThread.start();
		System.out.println("Socket created with multi-thread mechanism");

		if (!rs.first()){
			// 2.  entry doesn't exist
			FileChooser fc = new FileChooser(jdbcMySql);
			jdbcMySql.executeUpdate("INSERT UserGameTable (user_id, game_id, room_id, table_id,  seat_id, score, start_addr) VALUES (" +
					userID +
					", " +
					gameID +
					", " +
					roomID +
					", " +
					tableID +
					", " +
					seatID +
					", " +
					0 +
					", '" +
					fc.getSelectedFile().toString() +
					"'" +
			")");
			Runtime.getRuntime().exec("java -jar "+ fc.getSelectedFile().toString());
		}else{
			// 2.1. the entry has been existed
			try {
				Runtime.getRuntime().exec(rs.getString(1));
			} catch (IOException e) {
				System.err.println("Preset path of game is invalid");
				FileChooser fc = new FileChooser(jdbcMySql);
				try {
					Runtime.getRuntime().exec("java -jar "+fc.getSelectedFile().toString());
				} catch (IOException e1) {
					e1.printStackTrace();
				} 
			}
		}

		// 3. set status to be gaming
		System.out.println("UPDATE UserTable SET status='g' WHERE user_id=" +
				userID +
		";");
		System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE UserTable SET status='g' WHERE user_id=" +
				userID +
		";"));

		// 4. update the TBTable to set the user_id
		switch(seatID){
		case 1:
			System.out.println("UPDATE TBTable SET user1_id = " +
					userID +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";");
			System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE TBTable SET user1_id = " +
					userID +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";"));
			break;
		case 2:
			System.out.println("UPDATE TBTable SET user2_id = " +
					userID +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";");
			System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE TBTable SET user2_id = " +
					userID +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";"));
			break;
		case 3:
			System.out.println("UPDATE TBTable SET user3_id = " +
					userID +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";");
			System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE TBTable SET user3_id = " +
					userID +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";"));
			break;
		default: System.err.println("Error in update userID in TBTable");
		}

		// 5. Associate the seat with the nickname of user
		RefreshRightPanel(roomID, gameID);			

		// 6. refresh the Left Panel, in order to display correct amount of online users
		RefreshLeftPanel();
	}

	/** 
	 * The process of entering game from 14th group:
	 * 1. if the relevant entry in UserGameTable doesn't exist, create one
	 * 2. check whether current player is the fisrt player in the relevant table
	 * 3. 	 else open the game according to the path stored in the UserGameTable
	 * 	3.1.		if the path is incorrect, then invoke a file chooser update the path in table and then execute the program
	 * 4. set status to be gaming
	 * 5. update the TBTable to set the user_id
	 * 6. Associate the seat with the nickname of user
	 * 7. refresh the Left Panel, in order to display correct amount of online users
	 * 8. check whether the game has been terminated
	 * 
	 * @throws SQLException 
	 * @throws IOException 
	 */
	private void enter14Game() throws SQLException, IOException {
		boolean firstPlayerInTable;
		System.out.println("Enter Game");
		System.out.println("seat="+seatID+", tableID="+tableID+", roomID="+roomID+", gameID="+gameID);

		//  1. check the existence of the relevant entry in UserGameTable
		ResultSet rs = jdbcMySql.executeQuery("SELECT start_addr from UserGameTable where user_id = " +
				userID +
				" and game_id = " +
				gameID +
		"");

		// 2. check whether current player is the first player in the relevant table
		firstPlayerInTable = checkFirstPlayerInTable();
		System.out.println("Fisrt User In relevant table? "+firstPlayerInTable);

		rs = jdbcMySql.executeQuery("SElECT nickname, ip from UserTable where user_id=" +
				userID);
		if (rs.first()){
			userNickname = rs.getString(1);
			userIP = rs.getString(2);
		} else {
			System.err.println("ERROR: Fail to retrieve the nick name or ip of user "+userID);
			System.exit(-1);
		}
		
		// evoke game server for the first player
		if (firstPlayerInTable == true){
			Runtime.getRuntime().exec("GameServer.exe");
		}

		
		if (!rs.first()){
			// 2.  entry doesn't exist
			FileChooser fc = new FileChooser(jdbcMySql);
			jdbcMySql.executeUpdate("INSERT UserGameTable (user_id, game_id, room_id, table_id,  seat_id, score, start_addr) VALUES (" +
					userID +
					", " +
					gameID +
					", " +
					roomID +
					", " +
					tableID +
					", " +
					seatID +
					", " +
					0 +
					", '" +
					fc.getSelectedFile().toString() +
					"'" +
			")");
			gameStatus = Runtime.getRuntime().exec(fc.getSelectedFile().toString() +" " + userNickname + " " + userIP + " " + userPort);
			
			
		}else{
			// 2.1. the entry has been existed
			try {
				Runtime.getRuntime().exec(rs.getString(1));
			} catch (IOException e) {
				System.err.println("Preset path of game is invalid");
				FileChooser fc = new FileChooser(jdbcMySql);
				try {

					gameStatus = Runtime.getRuntime().exec(fc.getSelectedFile().toString()+" " + userNickname + " " + userIP + " " + userPort);
				} catch (IOException e1) {
					e1.printStackTrace();
				} 
			}
		
		}
		
		// 3. set status to be gaming
		System.out.println("UPDATE UserTable SET status='g' WHERE user_id=" +
				userID +
		";");
		System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE UserTable SET status='g' WHERE user_id=" +
				userID +
		";"));

		// 4. update the TBTable to set the user_id
		switch(seatID){
		case 1:
			System.out.println("UPDATE TBTable SET user1_id = " +
					userID +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";");
			System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE TBTable SET user1_id = " +
					userID +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";"));
			break;
		case 2:
			System.out.println("UPDATE TBTable SET user2_id = " +
					userID +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";");
			System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE TBTable SET user2_id = " +
					userID +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";"));
			break;
		case 3:
			System.out.println("UPDATE TBTable SET user3_id = " +
					userID +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";");
			System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE TBTable SET user3_id = " +
					userID +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";"));
			break;
		default: System.err.println("Error in update userID in TBTable");
		}

		// 5. Associate the seat with the nickname of user
		RefreshRightPanel(roomID, gameID);			

		// 6. refresh the Left Panel, in order to display correct amount of online users
		RefreshLeftPanel();
		
		// 7. check whether the game has been terminated
		while (true){
			try {
				while (gameStatus.exitValue()!=0){
					System.out.println("");
				}
				break;
			} catch (IllegalThreadStateException e) {				
			}
		}
		
		ExitGame(0, seatID, tableID, roomID, gameID);
		System.out.println("Game has exited");


	}

	private boolean checkFirstPlayerInTable() throws NumberFormatException, SQLException {
		int user1ID, user2ID, user3ID;

		System.out.println("SELECT user1_id, user2_id, user3_id FROM TBTable WHERE table_id = " +
				tableID +
				" and room_id = " +
				roomID +
				" and game_id = " +
				gameID +
		";");
		ResultSet rs=jdbcMySql.executeQuery("SELECT user1_id, user2_id, user3_id FROM TBTable WHERE table_id = " +
				tableID +
				" and room_id = " +
				roomID +
				" and game_id = " +
				gameID +
		";");


		if (rs.first()){				
			user1ID = Integer.parseInt(rs.getString(1));
			user2ID = Integer.parseInt(rs.getString(2));
			user3ID = Integer.parseInt(rs.getString(3));
			if (user1ID == 0 && user2ID == 0 && user3ID == 0){
				return true;
			} else{
				return false;
			}

		} else {
			System.err.println("ERROR: Fail to retrieve the user_id in the same table");
			System.exit(-1);
		}
		return false;
	}

	/* 
	 * Update information when user exits from game
	 * 
	 * 1. update game score
	 * 2. set status to "a"
	 * 3. update the TBTable to set the user_id
	 * 4. refresh the platform
	 */
	void ExitGame(int supposedScore, int seatID, int tableID, int roomID, int gameID) throws SQLException {
		System.out.println("Begin Exiting Program");

		// 1. update user's game table
		System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE UserGameTable SET room_id =0,table_id =0,seat_id =0,score=" +
				supposedScore +
				" WHERE user_id = " +
				userID +
		";"));

		// 2. set status to "a"
		System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE UserTable SET status = 'a' WHERE user_id = " +
				userID +
		";"));

		// 3. update the TBTable to set the user_id
		switch(seatID){
		case 1:
			System.out.println("UPDATE TBTable SET user1_id = " +
					0 +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";");
			System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE TBTable SET user1_id = " +
					0 +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";"));
			break;
		case 2:
			System.out.println("UPDATE TBTable SET user2_id = " +
					0 +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";");
			System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE TBTable SET user2_id = " +
					0 +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";"));
			break;
		case 3:
			System.out.println("UPDATE TBTable SET user3_id = " +
					0 +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";");
			System.out.println("Affected lines = " +jdbcMySql.executeUpdate("UPDATE TBTable SET user3_id = " +
					0 +
					" where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			";"));
			break;
		default: System.err.println("Error in update userID in TBTable");
		}

		// 4. Refresh the platform
		RefreshPlatform();		
	}

	/** 
	 * Refresh right Panel, according to the specified room and game
	 * @param gameID 
	 * @param roomID 
	 * @throws SQLException 
	 * 
	 */	
	private void RefreshRightPanel(int roomID, int gameID) throws SQLException {
		rightPanel.setBorder(BorderFactory.createTitledBorder(borderDisplay));
		rightPanel.removeAll();
		ResultSet rs=jdbcMySql.executeQuery("SELECT table_id, user1_id, user2_id, user3_id FROM TBTable where room_id = " +
				roomID +
				" and game_id = " +
				gameID +
		";");
		int TableID=0;
		if (rs.first()){
			do{
				rightPanel.add(
						newTableRightPanel(
								Integer.parseInt(rs.getString(2)),// user1_id
								Integer.parseInt(rs.getString(3)),// user2_id
								Integer.parseInt(rs.getString(4)),// user3_id
								++TableID
						));
			}while(rs.next());
		}

		//		rightPanel.removeAll();
		rightPanel.revalidate();
		rightPanel.repaint();
	}

	/** 
	 * check the availability of a particular seat
	 * 
	 * @param seatID
	 * @param tableID
	 * @param roomID
	 * @param gameID
	 * @return
	 * @throws SQLException
	 */
	private int checkUserInSeat(int seatID, int tableID, int roomID, int gameID) throws SQLException {
		ResultSet rs = null; 
		System.out.println("seat="+seatID+", tableID="+tableID+", roomID="+roomID+", gameID="+gameID);
		switch(seatID){
		case 1:
			rs = jdbcMySql.executeQuery("SELECT user1_id from TBTable where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			"");			
			break;
		case 2:
			rs = jdbcMySql.executeQuery("SELECT user2_id from TBTable where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			"");			
			break;
		case 3:
			rs = jdbcMySql.executeQuery("SELECT user3_id from TBTable where table_id = " +
					tableID +
					" and room_id = " +
					roomID +
					" and game_id = " +
					gameID +
			"");			
			break;
		default:
			System.err.println("The seat number passed in is incorrect.");
			System.exit(-1);
		}
		if (rs.first()){
			return Integer.parseInt(rs.getString(1));
		}else{
			System.err.println("Error occurs when checking the availability of seats.");
			System.exit(-1);
		}
		return -1;
	}

	/**
	 * Add a new instance of table to the right panel
	 * 
	 * @param user1
	 * @param user2
	 * @param user3
	 * @param tableID 
	 * @param TableID
	 * @param RoomID
	 * @param GameID
	 * @return
	 * @throws SQLException
	 */
	protected Component newTableRightPanel(final int user1,final int user2, final int user3, final int intableID) throws SQLException {

		JPanel tablePanel = new JPanel();
		SpringLayout layout = new SpringLayout();
		tablePanel.setLayout(layout);
		JLabel tableLabel = new JLabel(new ImageIcon(getClass().getResource("table.png")));
		final JLabel chairLabel1;
		final JLabel chairLabel2;
		final JLabel chairLabel3;
		final JLabel chairTextLabel1;
		JLabel chairTextLabel2;
		JLabel chairTextLabel3;

		ResultSet rs;

		// display nickname near the occupied table		
		rs = jdbcMySql.executeQuery("SElECT nickname from UserTable where user_id=" +
				user1);
		if (rs.first()){			
			
			chairTextLabel1 = new JLabel(rs.getString(1));			
			chairLabel1 = new JLabel(new ImageIcon(getClass().getResource("person.png")));
		}else{
			chairTextLabel1 = new JLabel();
			chairLabel1 = new JLabel(new ImageIcon(getClass().getResource("chair.png")));
		}

		rs = jdbcMySql.executeQuery("SElECT nickname from UserTable where user_id=" +
				user2);
		if (rs.first()){			
			chairTextLabel2 = new JLabel(rs.getString(1));
			chairLabel2 = new JLabel(new ImageIcon(getClass().getResource("person.png")));
		}else{
			chairTextLabel2 = new JLabel();
			chairLabel2 = new JLabel(new ImageIcon(getClass().getResource("chair.png")));
		}

		rs = jdbcMySql.executeQuery("SElECT nickname from UserTable where user_id=" +
				user3);
		if (rs.first()){
			chairTextLabel3 = new JLabel(rs.getString(1));
			chairLabel3 = new JLabel(new ImageIcon(getClass().getResource("person.png")));
		}else{
			chairTextLabel3 = new JLabel();
			chairLabel3 = new JLabel(new ImageIcon(getClass().getResource("chair.png")));
		}

		chairLabel1.addMouseListener(new MouseAdapter() {		
			public void mouseClicked(MouseEvent e) {
				mouseClicked1(e,intableID,chairLabel1);
			}
		});

		chairLabel2.addMouseListener(new MouseAdapter() {	
			public void mouseClicked(MouseEvent e) {
				mouseClicked2(e,intableID,chairLabel2);
			}
		});

		chairLabel3.addMouseListener(new MouseAdapter() {		
			public void mouseClicked(MouseEvent e) {
				mouseClicked3(e,intableID,chairLabel3);
			}
		});

		tablePanel.add(tableLabel);
		tablePanel.add(chairLabel1);
		tablePanel.add(chairTextLabel1);
		tablePanel.add(chairLabel2);
		tablePanel.add(chairTextLabel2);
		tablePanel.add(chairLabel3);
		tablePanel.add(chairTextLabel3);

		// Place Table
		layout.putConstraint(SpringLayout.WEST, tableLabel, 25, SpringLayout.WEST, tablePanel);
		layout.putConstraint(SpringLayout.NORTH, tableLabel, 25, SpringLayout.NORTH, tablePanel);

		// Place First Chair and the related user name
		layout.putConstraint(SpringLayout.WEST, chairLabel1, -10, SpringLayout.WEST, tableLabel);
		layout.putConstraint(SpringLayout.NORTH, chairLabel1, -10, SpringLayout.NORTH, tableLabel);
		layout.putConstraint(SpringLayout.WEST, chairTextLabel1, -10, SpringLayout.WEST, tableLabel);
		layout.putConstraint(SpringLayout.NORTH, chairTextLabel1, -25, SpringLayout.NORTH, tableLabel);

		// Place Second Chair and the related user name
		layout.putConstraint(SpringLayout.WEST, chairLabel2, -13, SpringLayout.EAST, tableLabel);
		layout.putConstraint(SpringLayout.SOUTH, chairLabel2, 13, SpringLayout.NORTH, tableLabel);
		layout.putConstraint(SpringLayout.WEST, chairTextLabel2, -13, SpringLayout.EAST, tableLabel);
		layout.putConstraint(SpringLayout.SOUTH, chairTextLabel2, -12, SpringLayout.NORTH, tableLabel);

		// Place Third Chair and the related user name
		layout.putConstraint(SpringLayout.WEST, chairLabel3, 25, SpringLayout.WEST, tableLabel);
		layout.putConstraint(SpringLayout.NORTH, chairLabel3, 0, SpringLayout.SOUTH, tableLabel);
		layout.putConstraint(SpringLayout.WEST, chairTextLabel3, 23, SpringLayout.WEST, tableLabel);
		layout.putConstraint(SpringLayout.NORTH, chairTextLabel3, 25, SpringLayout.SOUTH, tableLabel);		
		tablePanel.setPreferredSize(new java.awt.Dimension(144, 160));
		return tablePanel;
	}

	public void mouseClicked1(MouseEvent e, int inTableID, JLabel chairLabel1) {				 
		if (e.getClickCount() == 1) {
			System.out.println("Seat = 1, Table = "+inTableID+", Room ="+roomID+ ", Game ="+gameID);
			try {
				int userID = checkUserInSeat(1, inTableID, roomID, gameID);
				if(userID == 0 ){
					seatID = 1;
					tableID= inTableID;
					// seat on and join in the game
					chairLabel1.setIcon(new ImageIcon(getClass().getResource("person.png")));
					enterGame();
				} else{
					new MessageBox("Seat = 1, Table = "+inTableID+", Room ="+roomID+ ", Game ="+gameID +" is being occupied.");
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	public void mouseClicked2(MouseEvent e, int inTableID, JLabel chairLabel1) {				 
		if (e.getClickCount() == 1) {			
			System.out.println("Seat = 2, Table = "+inTableID+", Room ="+roomID+ ", Game ="+gameID);
			try {
				int userID = checkUserInSeat(2, inTableID, roomID, gameID);
				if(userID == 0 ){
					seatID = 2;
					tableID= inTableID;
					// seat on and join in the game
					chairLabel1.setIcon(new ImageIcon(getClass().getResource("person.png")));
					enterGame();
				} else{
					new MessageBox("Seat = 2, Table = "+inTableID+", Room ="+roomID+ ", Game ="+gameID +" is being occupied.");
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	public void mouseClicked3(MouseEvent e, int inTableID, JLabel chairLabel1) {				 
		if (e.getClickCount() == 1) {			
			System.out.println("Seat = 3, Table = "+inTableID+", Room ="+roomID+ ", Game ="+gameID);
			try {
				int userID = checkUserInSeat(3, inTableID, roomID, gameID);
				if(userID == 0 ){
					seatID = 3;
					tableID= inTableID;
					// seat on and join in the game
					chairLabel1.setIcon(new ImageIcon(getClass().getResource("person.png")));
					enterGame();
				} else{
					new MessageBox("Seat = 3, Table = "+inTableID+", Room ="+roomID+ ", Game ="+gameID +" is being occupied.");
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	/*
	 *  retrieval the user information from database, and display them in a label with HTML formation.
	 *  With gameID given, we will be able to get the all user's game information.
	 */
	public String AcquireUserInformation(int userID) throws SQLException {
		// TODO Auto-generated method stub
		ResultSet rs = jdbcMySql.executeQuery("SELECT account, nickname, sex, Age, status from UserTable WHERE user_id = " +
				userID +
		";");

		ResultSetMetaData md = rs.getMetaData();

		int cols = md.getColumnCount();
		String resultString = new String();
		if (rs.first()) {
			System.out.println(rs.getString(1));
			//			 <html> <body> line <br> newline </body> </html>
			resultString += "<html> <body> <center>";

			for (int i = 0; i < cols; i++) {
				resultString += md.getColumnName(i + 1) + ": \t" + rs.getString(i+1) + "<br>";
			}
		} else {
			new MessageBox("No record found for this user");
		}

		rs = jdbcMySql.executeQuery("SELECT game_id, room_id, table_id, seat_id, score from UserGameTable WHERE user_id = " +
				userID +
		";");

		md = rs.getMetaData();
		cols = md.getColumnCount();
		if (rs.first()) {
			System.out.println(rs.getString(1));
			for (int i = 0; i < cols; i++) {
				resultString += md.getColumnName(i + 1) + ": \t" + rs.getString(i+1) + "<br>";
			}
			resultString += "</center> </body> </html>";
		} else {
			new MessageBox("No record found for this user");
		}
		return resultString;		
	}
}

class RunSocket implements Runnable{
	private static SocketServer sc; 
	public void run(){
		try {
			sc = new SocketServer();
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public SocketServer getServerSocket(){
		return sc;
	}
}

class SocketThread extends Thread{
	private static SocketServer sc;
	int userID;
	int seatID;
	int tableID;
	int roomID;
	int gameID;
	GameHall gameHall;

	public SocketThread(GameHall ingameHall, int inuserID, int inseatID, int intableID, int inroomID, int ingameID){
		userID = inuserID;
		gameHall=  ingameHall;
		seatID = inseatID;
		tableID = intableID;
		roomID = inroomID;
		gameID = ingameID;
	}

	public void run(){
		sc = null;
		try {
			sc = new SocketServer();

			// 7. send user information to game, then wait for the response from game
			String numStr;
			int supposedScore = 0;

			ResultSet rs = gameHall.jdbcMySql.executeQuery("SELECT score from UserGameTable WHERE user_id = " +
					userID +
					" and game_id = " +
					gameID +
			";");
			if (rs.first()){
				supposedScore = Integer.parseInt(rs.getString(1));
			}else{
				System.err.println("ERROR: Can not retrieve the user game score");
				System.exit(-1);
			}

			sc.send(gameHall.AcquireUserInformation(userID));
			String comMess;

			do{
				comMess = sc.listen();
				System.out.println("Received Message is "+comMess);
				numStr = comMess.substring(1);
				if (!numStr.isEmpty()){
					supposedScore = Integer.parseInt(numStr);
					System.out.println("Supposed Score is "+supposedScore);	
				}				
			} while (comMess.startsWith("1"));

			// 8. exit Game process
			System.out.println("Game is about to exit");
			gameHall.ExitGame(supposedScore, seatID, tableID, roomID, gameID);
			sc.close();

			this.interrupt();

			System.out.println("Game has exited");


		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	public SocketServer getServerSocket(){
		return sc;
	}
}