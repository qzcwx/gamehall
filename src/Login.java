import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.imageio.ImageIO;
import javax.swing.JTextField;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;

import net.miginfocom.swing.MigLayout;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 * @param <JdbcMysql>
 */
public class Login extends javax.swing.JFrame {

	{
		//Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private static JdbcMySql jdbcMySql;

	private static Register RegWindow;

	private JTextField loginField;
	private JLabel loginLabel;
	private JLabel passwdLabel;
	private JPasswordField passwdField;
	private JButton loginButton;
	private JButton regButton;

	private String userName;
	private String passwd;
	private GameHall gameHall;

	private int userID;


	/**
	 * Auto-generated main method to display this JFrame
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Login inst=null;
				try {
					inst = new Login("Login",jdbcMySql);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}

	public Login(String string, JdbcMySql inJdbcMySql) throws MalformedURLException {
		jdbcMySql = inJdbcMySql;
		//		super(string,jdbcMySql); ???????
		this.setTitle(string);

		//			URL imagePath = new URL("file:GHall.png");
		//			URL imagePath=  new URL(getClass().getResource("src/GHall.png").toString());
		BufferedImage image = null;
		try {
			image = ImageIO.read(getClass().getResource("GHall.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.setIconImage(image);

		initGUI();
	}


	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			Container content = getContentPane();

			// Layout, Column and Row constraints as arguments.
			MigLayout layout = new MigLayout("fillx", "[right]rel[grow,fill]", "[]10[]");

			//			MigLayout layout = new MigLayout("fillx");
			content.setLayout(layout);


			loginLabel = new JLabel("User Name");
			loginField = new JTextField();

			passwdLabel = new JLabel("Password");			
			passwdField = new JPasswordField();

			loginButton = new JButton("Login");
			regButton = new JButton("Register");

			content.add(loginLabel);
			content.add(loginField,"wrap");

			content.add(passwdLabel);
			content.add(passwdField,"wrap");

			content.add(loginButton,"center,span");
			loginButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					try {
						loginButtonActionPerformed(evt);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});

			content.add(regButton,"center,span");
			regButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					regButtonActionPerformed(evt);
				}
			});

			pack();
			setSize(200, 170);
		} catch (Exception e) {
			//add your error handling code here
			e.printStackTrace();
		}
	}

	private void loginButtonActionPerformed(ActionEvent evt) throws SQLException, SocketException {
		//		Fetch the text once login button is pressed
		System.out.println("loginField="+loginField.getText());
		userName = loginField.getText();
		System.out.println("passwdField="+passwdField.getText());
		passwd = passwdField.getText();

		if (userName.isEmpty()||passwd.isEmpty()){
			new MessageBox("User Name/Password is empty!");
			return;
		}

		ResultSet rs = jdbcMySql.executeQuery("SELECT user_id from UserTable WHERE account = " +
				userName +
				" && password = " +
				passwd +
		";");

		if (rs.first()) {
			userID = Integer.parseInt(rs.getString(1));
			System.out.println(userID);
			System.out.println("Login Successfully");

			gameHall = new GameHall(userID,jdbcMySql);
			gameHall.setLocationRelativeTo(null);
			gameHall.setVisible(true);
			jdbcMySql.setStatus(userID, "'a'");
			this.dispose();
		} else {
			new MessageBox("User Name/Password incorrect!");
		}
		
		String clientIP =  "'" + getFirstNonLoopbackAddress().toString().substring(1) + "'";
		
		jdbcMySql.executeUpdate("UPDATE UserTable SET ip=" +
				clientIP +
				" WHERE user_id=" +
				userID+
				";");
	}

	private void regButtonActionPerformed(ActionEvent evt) {
		RegWindow = new Register("New User",jdbcMySql);
		RegWindow.setLocationRelativeTo(null);
		RegWindow.setVisible(true);	
	}
	
	private static InetAddress getFirstNonLoopbackAddress() throws SocketException {
		Enumeration en = NetworkInterface.getNetworkInterfaces();
		
		while (en.hasMoreElements()) {
			NetworkInterface i = (NetworkInterface) en.nextElement();
			for (Enumeration en2 = i.getInetAddresses(); en2.hasMoreElements();) {
			    InetAddress  addr = (InetAddress) en2.nextElement();
				if (!addr.isLoopbackAddress()&&addr instanceof Inet4Address) {
				    new MessageBox("Address = "+addr);
				    return addr; 
				}
			}
		}
		return null;
	}
}
