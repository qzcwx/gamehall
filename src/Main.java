import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.Spring;

/**
 * @author Wenxiang Chen
 */
public class Main {

	private static Login loginWindow;
	private final static String loginWelcomeText= "Login";
	private static JdbcMySql jdbcMysql;
	private final static int numberOfGame = 2;
	private final static String[] gameName = {"\"Game1\"","\"Game2\""};
	private final static int numberOfRoom = 3;
	private final static int numberOfTable = 30;
	private final static int numberOfSeat = 3;
	
	public static boolean reinitializeDB;
	public static String passwd;

	public static void main(String[] args) throws SQLException, IOException {

		PreConfig preConfig = new PreConfig();

		while(!preConfig.ready()){
			System.out.print("");
		}
		
		passwd = preConfig.getPasswd();
		reinitializeDB = preConfig.getReinitDB();
		
		System.out.println("Password for MySQL = " +passwd);
		System.out.println("Need re-initilized = " +reinitializeDB);
		
		connectToDB();
		//*****************************
		if (reinitializeDB == true){
			emptyMysqlDB();
			new MessageBox("Database Reinitialized, all previous data are cleaned");
		}
		//*****************************
		initMysqlDB();
		invokeLoginWindow();
	}

	private static void emptyMysqlDB() throws SQLException{
		jdbcMysql.executeUpdate("DROP DATABASE IF EXISTS GameHall;");		
	}

	private static void connectToDB() throws SQLException, IOException{
		//		Set up the JDBC connection to MySQL Database
		jdbcMysql = new JdbcMySql(passwd);
		if(jdbcMysql.beginSql() == false){
			System.exit(0);
		}
	}

	private static void invokeLoginWindow() throws MalformedURLException{
		loginWindow = new Login(loginWelcomeText,getMYSQL());
		//	Invoke Login Window
		loginWindow.setLocationRelativeTo(null);
		loginWindow.setVisible(true);
	}

	public static JdbcMySql getMYSQL(){
		return jdbcMysql;
	}

	/**
	 * Create Internal Tables in database, if any of them hasn't exist yet.
	 * 
	 * @return 
	 * @throws SQLException 
	 */
	private static void initMysqlDB() throws SQLException{
		//		Create Database with the name of GameHall
		jdbcMysql.executeUpdate("CREATE DATABASE IF NOT EXISTS GameHall;");
		//		Use newly created database
		jdbcMysql.executeUpdate("use GameHall;");

		//		Create Initial Tables
		jdbcMysql.executeUpdate("CREATE TABLE IF NOT EXISTS UserTable ( user_id INT, account VARCHAR(16), nickname VARCHAR(16), password VARCHAR(16), sex VARCHAR(1), Age INT, ip VARCHAR(256), status VARCHAR(1), primary key(user_id));");		
		jdbcMysql.executeUpdate("CREATE TABLE IF NOT EXISTS UserGameTable ( user_id  INT, game_id INT, room_id INT, table_id INT, seat_id INT, score INT, start_addr VARCHAR(256), primary key(user_id,game_id));");

		ResultSet rs=  jdbcMysql.executeQuery("SHOW TABLES LIKE \"GameTable\"");
		boolean initialized = false;
		if (!rs.first()){
			initialized = true;
		}
		jdbcMysql.executeUpdate("CREATE TABLE IF NOT EXISTS GameTable( game_id INT, game_name VARCHAR(256), game_rmnum INT, primary key(game_id));");
		if (initialized==true)
		{
			//			initialize game records, if it is not equal to the predefined number, then add records accordingly.
			int currentGameRecord = jdbcMysql.countNumOfRocord("game_id", "GameTable");
			int newGameID;
			for (int i=currentGameRecord; i<numberOfGame; i++){
				newGameID=i+1;
				jdbcMysql.executeUpdate("INSERT INTO GameTable (game_id, game_name, game_rmnum) VALUES (" +
						newGameID +
						", " +
						gameName[i] +
						", " +
						numberOfRoom +
				");");

				//				initialize rooms in a certain game
				jdbcMysql.executeUpdate("CREATE TABLE IF NOT EXISTS RoomTable( room_id INT, game_id INT, rm_online_num INT, seat_num INT, primary key(room_id,game_id));");
				int currentRoomRecord = jdbcMysql.countNumOfRocord("room_id", "RoomTable",newGameID),newRoomID=0;
				for (int j=currentRoomRecord; j<numberOfRoom; j++){
					newRoomID = j + 1;

					jdbcMysql.executeUpdate("INSERT INTO RoomTable (room_id, game_id, rm_online_num, seat_num) VALUES (" +
							newRoomID +
							", " +
							newGameID +
							", " +
							0 +
							",  " +
							numberOfSeat +
					");");

					//					initialize tables
					jdbcMysql.executeUpdate("CREATE TABLE IF NOT EXISTS TBTable( table_id INT, room_id INT,game_id INT, user1_id INT, user2_id INT, user3_id INT, primary key(table_id,room_id,game_id));");
					int currentTableRecord = jdbcMysql.countNumOfRocord("room_id", "RoomTable",newGameID,newRoomID),newTableID;

					for (int k=currentTableRecord; k<numberOfTable; k++){
						newTableID = k + 1;
						jdbcMysql.executeUpdate("INSERT INTO TBTable ( table_id , room_id ,game_id , user1_id , user2_id , user3_id ) VALUES (" +
								newTableID +
								", " +
								newRoomID +
								", " +
								newGameID +
						", 0, 0, 0);");
					}
				}
			}
		}
	}
}