

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;

import net.miginfocom.swing.MigLayout;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/

public class TestGame extends javax.swing.JFrame {

	JButton updateButton;
	JTextField supposeScoreTextField;
	JLabel userInfo;
	SocketClient gameSC;

	{
		//Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Auto-generated main method to display this JFrame
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				TestGame inst = new TestGame();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}

	public TestGame() {
		super();
		initGUI();
	}

	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

			this.addWindowListener(new java.awt.event.WindowAdapter() {
			    public void windowClosing(WindowEvent winEvt) {
			        // Perhaps ask user if they want to save any unsaved files first.
			    	gameSC.send(new String("0"));
			    	try {
						gameSC.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			        System.exit(0); 
			    }
			});
			
			Container content = getContentPane();
			this.setTitle("GAME");	

			gameSC = null;
			try {
				gameSC = new SocketClient();
			} catch(Exception e){
				new MessageBox("ERROR: Fail to connect to the server, exit!");
				System.exit(-1);
			}

			content.setLayout(new MigLayout("","[grow,center][center]"));
			userInfo = new JLabel(gameSC.listen());
			content.add(userInfo, "center, span");

			supposeScoreTextField =new JTextField();
			content.add(supposeScoreTextField,"growx");

			updateButton = new JButton("Update Score");
			content.add(updateButton);
			
			updateButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					updateButtonActionPerformed(evt);
				}
			});
			
			setSize(300,230);
//						pack();
		} catch (Exception e) {
			//add your error handling code here
			e.printStackTrace();
		}
	}

	/**
	 * Action when update button was pressed
	 * 1. get supposed score from text field
	 * 2. update the corresponding data in the remote server
	 * 3. 
	 * @param evt
	 */
	protected void updateButtonActionPerformed(ActionEvent evt) {
		// 1. get supposed score from text field
		int supposedScore = 0;
		supposedScore = Integer.parseInt(supposeScoreTextField.getText());
		
		// 2. update the corresponding data in the remote server by sending out socket
		String sendMsg = new String("1"+Integer.toString(supposedScore));
		System.out.println("Message aobut to be sent: "+sendMsg);
		gameSC.send(sendMsg);
		new MessageBox("Message "+sendMsg+ " has been send to game hall");
	}

}
