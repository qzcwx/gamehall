import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Enumeration;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;

import net.miginfocom.swing.MigLayout;

public class Register extends javax.swing.JFrame {

	private static JdbcMySql jdbcMySql;

	private JLabel userNameLabel;
	private JTextField userNameField;
	private JLabel nickNameLabel;
	private JTextField nickNameField;
	private JTextField passwdField;
	private JLabel passwdLabel;
	private JLabel genderLabel;
	private JTextField ageField;
	private JLabel ageLabel;
	private JComboBox genderList;

	private int userID;
	private String clientIP;
	private int age;
	private String gender;
	private String nickName;
	private String userName;
	private String passwd;
	private String status; // O: Off-line; G: Gaming; A:Available.


	//	public static void main(String[] args) {
	//		SwingUtilities.invokeLater(new Runnable() {
	//			public void run() {
	//				Register inst = new Register("New User",jdbcMySql);
	//				inst.setLocationRelativeTo(null);
	//				inst.setVisible(true);
	//			}
	//		});
	//	}

	public Register(String string, JdbcMySql inJdbcMySql) {
		this.jdbcMySql = inJdbcMySql;		
		this.setTitle(string);

		// set the icon for window
		try {
			URL imagePath = new URL("file:GHall.png");
			BufferedImage image = null;
			try {
				image = ImageIO.read(getClass().getResource("GHall.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.setIconImage(image);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}		
		initGUI();
	}

	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			Container content = getContentPane();
			MigLayout layout = new MigLayout("fillx", "[right]rel[grow,fill]", "[]10[]");
			content.setLayout(layout);


			// Registration Form Area

			userNameLabel = new JLabel("User Name");			
			userNameField = new JTextField();
			passwdLabel = new JLabel("Password");
			passwdField = new JPasswordField();
			nickNameLabel = new JLabel("Nick Name");
			nickNameField = new JTextField();
			genderLabel = new JLabel("Gender");

			String[] genderStrings = {"Not Selected", "Male", "Female"};
			genderList = new JComboBox(genderStrings);
			genderList.setSelectedIndex(0);

			ageLabel = new JLabel("Age");
			ageField = new JTextField();

			content.add(userNameLabel);
			content.add(userNameField,"wrap");
			content.add(passwdLabel);
			content.add(passwdField,"wrap");
			content.add(nickNameLabel);
			content.add(nickNameField,"wrap");
			content.add(genderLabel);
			content.add(genderList,"wrap");
			content.add(ageLabel);
			content.add(ageField,"wrap");

			// Confirm Button
			JButton confirmButton = new JButton("Create Account");
			confirmButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					try {
						try {
							confirmButtonActionPerformed(evt);
						} catch (SQLException e) {
							e.printStackTrace();
						}
					} catch (SocketException e) {
						e.printStackTrace();
					}
				}
			});

			content.add(confirmButton,"center,span");


			pack();
			setSize(275, 240);
		} catch (Exception e) {
			//add your error handling code here
			e.printStackTrace();
		}
	}

	protected void confirmButtonActionPerformed(ActionEvent evt) throws SocketException, SQLException{

		System.out.println("userName="+
				userNameField.getText());
		if ( userNameField.getText().isEmpty()){
			new MessageBox("User Name Field is empyty!");
			return; 
		}
		userName = "'"+ userNameField.getText() +"'";

		if ( passwdField.getText().isEmpty()){
			new MessageBox("Password Field is empyty!");
			return; 
		}
		System.out.println("passwdField="+
				passwdField.getText());
		passwd ="'"+ passwdField.getText()+"'";

		System.out.println("nickName="+
				nickNameField.getText());
		if ( nickNameField.getText().isEmpty()){
			new MessageBox("Nickname Field is empyty!");
			return; 
		}
		nickName ="'"+ nickNameField.getText() +"'";


		System.out.println("gender="+
				genderList.getSelectedItem().toString());
		gender = "'"+ genderList.getSelectedItem().toString().substring(0, 1) + "'";

		System.out.println("age="+
				ageField.getText());
		if (!ageField.getText().isEmpty() && isIntNumber(ageField.getText())){
			age = Integer.parseInt(ageField.getText());	
		} else{
			new MessageBox("Age Field is empty or filled in incorrectly");
			return;
		}

		clientIP  = "'" + getFirstNonLoopbackAddress().toString().substring(1) + "'";

		userID = jdbcMySql.countNumOfRocord("user_id","UserTable") + 1;
		status = "'o'";
		addRecordToUserTable();

		this.dispose();
	}


	//	private int getUserID() throws SQLException {
	//		ResultSet rs = jdbcMySql.executeQuery("Select COUNT(user_id) from UserTable;");
	//		if (rs.first()) {
	//			do {
	//				// System.out.println(rs.getString("name"));
	//				return Integer.parseInt(rs.getString(1));
	//			} while (rs.next());
	//		}
	//		return -1;
	//	}

	/**
	 * Add a piece of record to the UserTable
	 * @throws SQLException 
	 */
	private void addRecordToUserTable() throws SQLException {
		int lineEffected = jdbcMySql.executeUpdate("INSERT INTO UserTable (user_id, account, nickname, password, sex, Age, ip, status) VALUES (" +
				userID +
				", " +
				userName +
				", " +
				nickName +
				", " +
				passwd +
				", " +
				gender +
				", " +
				age +
				", " +
				clientIP +
				", " +
				status +
		");");

		System.out.println("lineEffected = "+ lineEffected);
	}

	/**
	 * Accessory Method for detecting the correct IP address
	 * 
	 * @param preferIpv4
	 * @param preferIPv6
	 * @return
	 * @throws SocketException
	 */
	private static InetAddress getFirstNonLoopbackAddress() throws SocketException {
		Enumeration en = NetworkInterface.getNetworkInterfaces();
		
		while (en.hasMoreElements()) {
			NetworkInterface i = (NetworkInterface) en.nextElement();
			for (Enumeration en2 = i.getInetAddresses(); en2.hasMoreElements();) {
			    InetAddress  addr = (InetAddress) en2.nextElement();
				if (!addr.isLoopbackAddress()&&addr instanceof Inet4Address) {
				    new MessageBox("Address = "+addr);
				    return addr; 
				}
			}
		}
		return null;
	}

	public boolean isIntNumber(String num){
		try{
			Integer.parseInt(num);
		} catch(NumberFormatException nfe) {
			return false;
		}
		return true;
	}

}
