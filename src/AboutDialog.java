

import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;

import net.miginfocom.swing.MigLayout;


/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class AboutDialog extends javax.swing.JDialog {
	private JLabel jLabelIcon;
	private JLabel jLabelName;
	private JLabel jLabelLink;



	/**
	 * Auto-generated main method to display this JFrame
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				AboutDialog inst = new AboutDialog();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}

	public AboutDialog() {
		super();

		// set the icon for window
		try {
			URL imagePath = new URL("file:GHall.png");
			BufferedImage image = null;
			try {
				image = ImageIO.read(getClass().getResource("GHall.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.setIconImage(image);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		initGUI();
	}

	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

			Container content = getContentPane();
			content.setLayout(new MigLayout("center"));
			
//			URL iconPath = new URL("file:GHall.png");
//	        ImageIcon vv=new ImageIcon(iconPath);
			
//			jLabelIcon = new JLabel(scale(new ImageIcon("./GHall.png").getImage(), 1));
			jLabelIcon = new JLabel(new ImageIcon(getClass().getResource("GHall.png")));
			content.add(jLabelIcon,"center");
			
			content.add(new JLabel("<html> <body> <center> <h1> <strong>GHAll</strong> </h1> <p>A Java-Based Cross-Platform Game Hall</p> <em>Copyright 2010 by Group 33</em><br> <br> Wenxiang Chen (chenwx.ustc@gmail.com)<br> Wenjian Huang (huangwj@mail.ustc.edu.cn)<br> Chao Zhong (...)<br> Mi Fang <br>  <br> </center> </body> </html>"),"wrap,center");
			
			jLabelLink = new JLabel("<html> <body> <center> <a href=\"http://cs-chen.net\" >http://cs-chen.net</a> </center> </body> </html>");
			final DesktopRuner dr =  new DesktopRuner("http://cs-chen.net");
			dr.changeMouse(jLabelLink);
			jLabelLink.addMouseListener(new MouseListener() {
				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub					
					dr.runBroswer();			
				}

				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub					
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub					
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub					
				}

				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub					
				}
			});

			content.add(jLabelLink,"center");
			
			pack();
			setSize(450, 250);
		} catch (Exception e) {
			//add your error handling code here
			e.printStackTrace();
		}
	}

//	private ImageIcon scale(Image src, double scale) {
//		 int w = (int)(scale*src.getWidth(this));
//	        int h = (int)(scale*src.getHeight(this));
//	        int type = BufferedImage.TYPE_INT_RGB;
//	        BufferedImage dst = new BufferedImage(w, h, type);
//	        Graphics2D g2 = dst.createGraphics();
//	        g2.drawImage(src, 0, 0, w, h, this);
//	        g2.dispose();
//	        return new ImageIcon(dst);
//	}
}
