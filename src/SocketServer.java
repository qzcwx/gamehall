import java.net.*;
import java.io.*;

public class SocketServer
{
	ServerSocket aServerSocket;
	Socket aServer;

	public SocketServer() throws UnknownHostException,IOException,ConnectException{
		aServerSocket=new ServerSocket(3434);
		aServer=null;
		try
		{		
			aServer=aServerSocket.accept();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	/* 
	 * Send the inSpring to server
	 */
	public void send(String inString) {
		try {
			PrintWriter out=new PrintWriter(new OutputStreamWriter(aServer.getOutputStream()));
			out.println(inString);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 *  read from client via socket
	 */
	public String listen() {
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(aServer.getInputStream()));
			return in.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/* 
	 * close created socket for future use
	 */
	public void close() throws IOException {
		aServerSocket.close();		
		aServer.close();
	}


	//	public static void main(String[] args) throws UnknownHostException,IOException,ConnectException
	//	{
	//		System.out.println("main");
	//		System.out.println("c");
	//		ServerSocket aServerSocket=new ServerSocket(3434);
	//		Socket aServer=null;
	//		try
	//		{
	//			System.out.println("b");
	//			aServer=aServerSocket.accept();
	//			System.out.println("a");
	//			try
	//			{
	//				BufferedReader input=new BufferedReader(new InputStreamReader(System.in));
	//				BufferedReader in=new BufferedReader(new InputStreamReader(aServer.getInputStream()));
	//				PrintWriter out=new PrintWriter(new OutputStreamWriter(aServer.getOutputStream()));
	//
	//				String serverstring=null;
	//				String clientstring=null;
	//				System.out.println("hello! enter the bye to exit.");
	//				System.out.print("Server:wait client");
	//				serverstring=input.readLine();
	//				boolean done=false;
	//				while(!done)
	//				{
	//					if(serverstring !=null)
	//					{
	//						out.println(serverstring);
	//						out.flush();
	//					}
	//					clientstring=in.readLine();
	//					if(clientstring !=null)
	//						System.out.println("client:"+clientstring);
	//					System.out.print("server:");
	//					serverstring=input.readLine();
	//					if(serverstring.equals("bye")) done=true;
	//				}
	//			}
	//			catch(Exception e)
	//			{
	//				System.out.println(e.getMessage());
	//			}
	//			finally
	//			{
	//				aServer.close();
	//			}
	//		}
	//		catch(Exception e)
	//		{
	//			System.out.println(e.getMessage());
	//		}
	//		finally
	//		{
	//			aServerSocket.close();
	//		}
	//	}
}