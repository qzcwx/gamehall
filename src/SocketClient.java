import java.net.*;
import java.io.*;


public class SocketClient
{
	static Socket aClient;
	BufferedReader in;
	PrintWriter out;
	
	public SocketClient() throws UnknownHostException, IOException {
		aClient=new Socket("127.0.0.1",3434); //InetAddress.getLocalHost()
	}
	
	// read from server via socket
	public String listen() {
		try {
			in=new BufferedReader(new InputStreamReader(aClient.getInputStream()));
			return in.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/* 
	 * Send the inSpring to server
	 */
	public void send(String inString) {
		try {
			out=new PrintWriter(new OutputStreamWriter(aClient.getOutputStream()));
			out.println(inString);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void close() throws IOException{
		in.close();
		out.close();
		aClient.close();
	}
	
//	public static void main(String[] args) throws UnknownHostException,IOException,ConnectException
//	{
//		
//		aClient=new Socket("127.0.0.1",3434); //InetAddress.getLocalHost()
//		
//		try
//		{
//			BufferedReader input=new BufferedReader(new InputStreamReader(System.in));
//			BufferedReader in=new BufferedReader(new InputStreamReader(aClient.getInputStream()));
//			PrintWriter out=new PrintWriter(new OutputStreamWriter(aClient.getOutputStream()));
//
//			String clientString=null;
//			String serverString=null;
//			System.out.println("hello!enter bye to exit.");
//			boolean done=false;
//			while(!done)
//			{
//				serverString=in.readLine();
//				if(serverString !=null)
//					System.out.println("Server:"+serverString);
//				System.out.print("client:");
//				clientString=input.readLine();
//				if(clientString.equals("bye")) done=true;
//				if(clientString !=null)
//				{
//					out.println(clientString);
//					out.flush();
//				}
//			}
//			in.close();
//			out.close();
//		}
//		catch(Exception e)
//		{
//			System.out.println(e.getMessage());
//		}
//		
//		finally
//		{
//			aClient.close();
//		}
//	}
} 