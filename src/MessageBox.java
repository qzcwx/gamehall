import   javax.swing.JOptionPane;

public   class   MessageBox
{
	{
		//Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public MessageBox(String string) {		
		JOptionPane.showMessageDialog(null, string);
	}
}