

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.SocketException;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.Spring;
import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;

import net.miginfocom.swing.MigLayout;

public class PreConfig extends javax.swing.JFrame {

	{
		//Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private JLabel passwdLabel;
	private JPasswordField passwdField;
	private JLabel resetLabel;
	private JComboBox resetList;
	private JButton beginButton;
	private boolean ready;
	private JLabel adminPasswdLabel;
	private JPasswordField adminPasswdField;
	
	private static boolean reinitializeDB;
	private static String passwd;
	
	private static String adminPasswd = "ustc";
	
	public boolean getReinitDB(){
		return reinitializeDB;
	}
	
	public String getPasswd(){
		return passwd;
	}
	
	public PreConfig() {
		
		super();
		ready = false;
		initGUI();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	

	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			
			Container content = getContentPane();
			MigLayout layout = new MigLayout("fillx", "[right]rel[grow,fill]", "[]10[]");

			content.setLayout(layout);

			passwdLabel = new JLabel("Password for MySQL");
			passwdField = new JPasswordField();

			content.add(passwdLabel);
			content.add(passwdField,"wrap");
			
			resetLabel = new JLabel("Reset Database");
			String[] genderStrings = {"No", "Yes"};
			resetList = new JComboBox(genderStrings);
			resetList.setSelectedIndex(0);
			
			content.add(resetLabel);
			content.add(resetList,"wrap");
			
			adminPasswdLabel = new JLabel("<html><body>Administrative Password<br>(if request reinitialization)</body></html>");
			adminPasswdField = new JPasswordField();

			content.add(adminPasswdLabel);
			content.add(adminPasswdField,"wrap");
			
			beginButton = new JButton("Begin");
			content.add(beginButton,"span");
			
			beginButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					beginButtonActionPerformed(evt);
				}
			});
			
			pack();
			setSize(300, 200);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void beginButtonActionPerformed(ActionEvent evt) {
		if ( passwdField.getText().isEmpty()){
			new MessageBox("Password Field is empyty!");
			return; 
		}
		System.out.println("passwdField="+
				passwdField.getText());
		passwd =passwdField.getText();
		
		System.out.println("reset="+
				resetList.getSelectedItem().toString());
		String selectedItem = resetList.getSelectedItem().toString();
		if (selectedItem.equals("Yes")){
			if (adminPasswdField.getText().equals(adminPasswd)){
				// input password is correct
				reinitializeDB = true;
				System.out.println("Ready = "+ ready);
				ready = true;
				this.dispose();
			} else{
				// password incorrect prompt error message
				new MessageBox("Administrative Password is incorrect, you're forbidden to reset database");
			}
						
		}else{
			reinitializeDB = false;
			System.out.println("Ready = "+ ready);
			ready = true;
			this.dispose();
		}
		
	}
	 
	public boolean ready(){
		return ready;
	}
	
	

}
