import java.awt.Cursor;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.swing.JLabel;

/**
 * this class is responsible 
 * @netSite 指定要显示的网址
 */
public class DesktopRuner {
	private Desktop desktop;
	private URI uri;
	private String netSite;
	private Cursor hander;
	
	/** Creates a new instance of DesktopRuner */
	public DesktopRuner(String link) {
		netSite = link;
		this.desktop = Desktop.getDesktop();
	}
	
	/*
	 * check whether there is a installed browser 
	 */
	public boolean checkBroswer(){
		if(Desktop.isDesktopSupported() && desktop.isSupported(Desktop.Action.BROWSE)){
			return true;
		}
		else{
			return false;
		}
	}
	
	/*
	 * run the default browser 
	 */
	public void runBroswer(){
		try {
			uri = new URI(netSite);
		} catch (URISyntaxException ex) {
			ex.printStackTrace();
		}
		try {
			desktop.browse(uri);			
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	/**	
	 * Open the file with associated application
	 */
	public void openFile() throws URISyntaxException{
		File uri;
		uri = new File(netSite);
		try {
			desktop.open(uri);		
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	/*
	 * change the shape of mouse
	 */
	public void changeMouse(JLabel label){
		hander = new Cursor(Cursor.HAND_CURSOR);
		label.setCursor(hander);
	}
}