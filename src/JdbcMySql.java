import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.Spring;

public class JdbcMySql {
	private static Connection connect;
	private static Statement stmt;
	String passwd;
	public JdbcMySql(String input) {
		passwd = input;
		// JDBC default constructor
		try {
			Class.forName("com.mysql.jdbc.Driver"); // 鍔犺浇MYSQL JDBC椹卞姩绋嬪簭
			System.out.println("Success loading Mysql Driver!");
		} catch (Exception e) {
			System.out.println("Error loading Mysql Driver!");
			e.printStackTrace();
		}
	}

	public boolean beginSql() {
		try {
			connect = DriverManager.getConnection(		
					"jdbc:mysql://localhost:3306", "root", passwd);
			// 杩炴帴URL涓�jdbc:mysql//鏈嶅姟鍣ㄥ湴鍧�鏁版嵁搴撳悕
			// 鍚庨潰鐨�涓弬鏁板垎鍒槸鐧婚檰鐢ㄦ埛鍚嶅拰瀵嗙爜

			System.out.println("Success connect Mysql server!");
			return true;

			// sqlProcessing(connect);
		} catch (Exception e) {
			// exception procedure for problems of connecting the database
			e.printStackTrace();
			return false;
		}
	}

	/*
	public static void execute() throws SQLException,
	IOException {
		do {
			System.out
			.println("Please insert your query command: (1. Query; 2. Update; 0. Quit)");
			BufferedReader stdin = new BufferedReader(new InputStreamReader(
					System.in));
			String inPutLine = stdin.readLine();
			if (inPutLine.length() != 1) {
				continue;
			}

			String inStr = inPutLine.toString();
			long costtime = 0;
			if (inStr.equals("1")) {
				System.out.println("Query Mode");
				inPutLine = stdin.readLine();
				ResultSet rs = null;
				try {
					long start = System.currentTimeMillis();
					rs = stmt.executeQuery(inPutLine);
					long end = System.currentTimeMillis();
					costtime = end - start;
				} catch (Exception e) {
					System.out.println("Syntax Error!");
					e.printStackTrace();
				}
				if (rs.first()) {
					do {
						// System.out.println(rs.getString("name"));
						System.out.println(rs.getString(1));
					} while (rs.next());
				}
				System.out.println("Processing time is " + costtime + "ms");
				System.out
				.println("*******************************************");
			} else if (inStr.equals("2")) {
				System.out.println("Update Mode");
				inPutLine = stdin.readLine();
				System.out
				.println("*******************************************");
				int lineAffected = 0;
				try {
					lineAffected = stmt.executeUpdate(inPutLine);
				} catch (Exception e) {
					System.out.println("Syntax Error!");
					e.printStackTrace();
				}
				System.out.printf("%d row(s) affected\n", lineAffected);
				System.out
				.println("*******************************************");
			} else if (inStr.equals("0")) {
				break;
			}
		} while (true);	
	}
	 */

	public int executeUpdate(String string) throws SQLException{
		stmt = connect.createStatement();
		if (stmt.isClosed()) {
			System.out.println("Connection is closed.");
			System.exit(-1);
		}
		if (this.getStatement().isClosed()) {
			System.out.println("Connection is closed.(adminSQL)");
			System.exit(-1);
		}
		return stmt.executeUpdate(string);
	}

	public ResultSet executeQuery(String string) throws SQLException{
		stmt = connect.createStatement();
		if (stmt.isClosed()) {
			System.out.println("Connection is closed.");
			System.exit(-1);
		}
		if (this.getStatement().isClosed()) {
			System.out.println("Connection is closed.(adminSQL)");
			System.exit(-1);
		}
		return stmt.executeQuery(string);
	}

	public Connection getConnection() {
		return connect;
	}
	public Statement getStatement() {
		return stmt;
	}

	/**
	 * Set the status of the user with specific ID,
	 * o: off-line;
	 * a: available
	 * g: gaming
	 * 
	 * @param ID
	 * @param status
	 * @throws SQLException 
	 */
	public void setStatus(int ID, String status) throws SQLException{
		System.out.println("UPDATE UserTable SET status=" +
				status +
				" WHERE user_id=" +
				ID +
				";");
		stmt.executeUpdate("UPDATE UserTable SET status=" +
				status +
				" WHERE user_id=" +
				ID +
				";");
	}
	
	/**
	 * Count the amount of records from specified table with specified field  
	 * @return
	 * @throws SQLException
	 */
	int countNumOfRocord(String fieldName, String tableName) throws SQLException {
		ResultSet rs = executeQuery("Select COUNT(" +
				fieldName +
				") from " +
				tableName +
				";");
		if (rs.first()) {
			do {		
				return Integer.parseInt(rs.getString(1));
			} while (rs.next());
		}
		return -1;
	}

	int countNumOfRocord(String fieldName, String tableName, int ID) throws SQLException {
		System.out.println("Select COUNT(" +
				fieldName +
				") from " +
				tableName + "where game_id = " +
				ID +
				";");
		ResultSet rs = executeQuery("Select COUNT(" +
				fieldName +
				") from " +
				tableName + " where game_id = " +
				ID +
				";");
		if (rs.first()) {
			do {		
				return Integer.parseInt(rs.getString(1));
			} while (rs.next());
		}
		return -1;
	}

	public int countNumOfRocord(String string, String string2, int newGameID,
			int newRoomID) {
		// TODO Auto-generated method stub
		return 0;
	}
}
